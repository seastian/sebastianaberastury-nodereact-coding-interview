import people_data from "../data/people_data.json";

type Offset = { tag: "Offset"; value: number };

export class PeopleProcessing {
  getById(id: number) {
    return people_data.find((p) => p.id === id);
  }

  getAll(searchTerm: string) {
    return people_data.filter((i) => i.last_name.includes(searchTerm));
  }
}
