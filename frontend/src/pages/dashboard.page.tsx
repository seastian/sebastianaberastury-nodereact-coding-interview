import React, { FC, useState, useEffect } from "react";

import { RouteComponentProps } from "@reach/router";
import { IUserProps } from "../dtos/user.dto";
import { UserCard } from "../components/users/user-card";
import { CircularProgress } from "@mui/material";

import { BackendClient } from "../clients/backend.client";
import TextField from "@mui/material/TextField";

const backendClient = new BackendClient();

export const DashboardPage: FC<RouteComponentProps> = () => {
  const [users, setUsers] = useState<IUserProps[]>([]);
  const [isLoading, setIsLoading] = useState(true);
  const [searchTerm, setSearchTerm] = useState("");

  useEffect(() => {
    const fetchData = async () => {
      const result = await backendClient.getAllUsers(searchTerm);
      setUsers(result.data);
      setIsLoading(false);
    };
    setIsLoading(true);
    fetchData();
  }, [searchTerm]);

  return (
    <div style={{ paddingTop: "30px" }}>
      <TextField
        value={searchTerm}
        onChange={(evt) => {
          setSearchTerm(evt.target.value);
        }}
      />
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        {isLoading ? (
          <div
            style={{
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
              height: "100vh",
            }}
          >
            <CircularProgress size="60px" />
          </div>
        ) : (
          <div>
            {users.length
              ? users.map((user) => {
                  return <UserCard key={user.id} {...user} />;
                })
              : null}
          </div>
        )}
      </div>
    </div>
  );
};
